//
//  PaddingLabel.swift
//  Appify
//
//  Created by Kapil Babani on 30/08/18.
//  Copyright © 2018 Ux_Army. All rights reserved.
//

import Foundation
import UIKit
class PaddingLabel : UILabel{
    
    let padding = UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10)
    override func drawText(in rect: CGRect) {
        super.drawText(in: UIEdgeInsetsInsetRect(rect, padding))
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        let superSizeThatFits = super.sizeThatFits(size)
        let width = superSizeThatFits.width + padding.left + padding.right
        let height = superSizeThatFits.height + padding.bottom + padding.top
        return CGSize(width: width, height: height)
    }
    
}
