//
//  AppDelegate.swift
//  Appify
//
//  Created by Kapil Babani on 14/08/18.
//  Copyright © 2018 Ux_Army. All rights reserved.
//

import UIKit
import GoogleSignIn
import CoreData
import SwiftyJSON

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate{
    
    var window: UIWindow?
    var email = ""
    var fullName = ""
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        //google login
        
        GIDSignIn.sharedInstance().clientID = "195066370747-j96n5a1oelfts5tq12nmbj7r8urn22cp.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        
        //facebook login
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        if UserDefaults.standard.string(forKey: "UserID") != nil
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "Product")
            self.window?.rootViewController = initialViewController
            
        }
        self.window?.makeKeyAndVisible()
        
        return true
    }
    
    //MARK:- GOOGLE SIGN IN
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!)
    {
        if let error = error {
            print("\(error.localizedDescription)")
        }
            
        else{
            let userId = user.userID                  // For client-side use only!
            //let idToken = user.authentication.idToken // Safe to send to the server
            fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            email = user.profile.email
            print(userId!, fullName, givenName!, familyName!, email)
            googleUserRegister()
        }
    }
    
    //Google user check and register
    func googleUserRegister(){
        
        //core data intialization and entering value in entity
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let userEntity = NSEntityDescription.entity(forEntityName: "User", in: managedContext)!
        let user = NSManagedObject(entity: userEntity, insertInto: managedContext)
        
        //check if user already exist google
        func someUserExists() -> Bool {
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "User")
            fetchRequest.predicate = NSPredicate(format: "email = %@", email)
            
            var results: [NSManagedObject] = []
            
            do {
                results = try managedContext.fetch(fetchRequest)
            }
            catch {
                print("error executing fetch request: \(error)")
            }
            return results.count > 0
        }
        
        // if user alreay exist Google
        if someUserExists(){
            print("user already exist")
            UserDefaults.standard.set(email, forKey: "UserID")
            print(UserDefaults.standard.string(forKey: "UserID")!)
            
        }
        else {
            //saving data to data model
            
            do {
                //input data in entity on succ.
                
                user.setValue(fullName, forKeyPath: "name")
                user.setValue(email, forKey: "email")
                try managedContext.save()
                print("user registered")
                UserDefaults.standard.set(email, forKey: "UserID")
                print(UserDefaults.standard.string(forKey: "UserID")!)
            }
            catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
    }
    
    //facebook login app delegates
    
    private func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool
    {
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool
    {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    
    //google login sdk delegates
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool
    {
        return GIDSignIn.sharedInstance().handle(url as URL?,sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,annotation: options[UIApplicationOpenURLOptionsKey.annotation])
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        

        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {

        // local file accessing
//        if let path = Bundle.main.path(forResource: "Product", ofType: "json") {
//            do {
//                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
//                print(data)
//                let productData = try JSON(data: data)
//                if let productId = productData[0]["product_id"].string {
//                    print(productId)
//                }
//                else {
//                    print("")
//                }
//                print("jsonData:\(productData)")
//            } catch let error {
//                print("parse error: \(error.localizedDescription)")
//            }
//        } else {
//            print("Invalid filename/path.")
//        }
        
        // api json file accessing
        struct color_nippon: Decodable{
            let code: String
            let name: String
            let value: String
            let category: String
            let family: String
            let display_order: Int
        }
        
        guard let colorUrl = URL(string: "http://nipponapi.uxarmy.com/rest/rgb-colours/?format=json") else { return }
        URLSession.shared.dataTask(with: colorUrl) { (data, response
            , error) in
            guard let data = data else { return }
            do {
                let decoder = JSONDecoder()
                let colorData = try decoder.decode(color_nippon.self, from: data)
                print(colorData.name)
                
            } catch let err {
                print("Err", err)
            }
            }.resume()
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Appify")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}






