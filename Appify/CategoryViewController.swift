//
//  CategoryViewController.swift
//  Appify
//
//  Created by Kapil Babani on 16/08/18.
//  Copyright © 2018 Ux_Army. All rights reserved.
//

import UIKit
let reuseIdentifier = "CategoryCellId";
class CategoryViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var Array = [String]()
    var logoImages: [UIImage] = []
    var start = integer_t()
    var end = integer_t()
    
    @IBOutlet weak var CategoryCollection: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for i in start...end{
            Array.append("product \(i)")
            logoImages.append(UIImage(named: "Product_\(i).png")!)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (Array.count)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as UICollectionViewCell
        cell.backgroundColor = self.randomColor()
        let Button = cell.viewWithTag(1) as! UILabel
        Button.text = Array[indexPath.row]
        
        let ButtonAct = cell.viewWithTag(2) as! UIButton
        ButtonAct.setImage(logoImages[indexPath.row], for: UIControlState.normal)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let pro_desVC = self.storyboard?.instantiateViewController(withIdentifier: "ProductDescrip") as! ProductDes
        
        //image data and label data passwd
        
        pro_desVC.getImage = logoImages[indexPath.row]
        pro_desVC.getName = Array[indexPath.row]
        print( pro_desVC.getImage)
        
        self.present(pro_desVC, animated: true, completion: nil)
    }
    
    //MARK:- categories BUTTON
    
    @IBAction func categoriesButton(_ sender: Any)
    {
        let provc = self.storyboard?.instantiateViewController(withIdentifier: "Product") as! ProductsView
        self.present(provc, animated: true, completion: nil)
        
    }
    
    //MARK:- cart BUTTON
    
    @IBAction func cartButton(_ sender: Any)
    {
        let cartvc = self.storyboard?.instantiateViewController(withIdentifier: "cart") as! CartView
        self.present(cartvc, animated: true, completion: nil)
    }
    
    //MARK:- BACK BUTTON
    
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    func randomColor() -> UIColor{
        let red = CGFloat(drand48())
        let green = CGFloat(drand48())
        let blue = CGFloat(drand48())
        return UIColor(red: red, green: green, blue: blue, alpha: 1.0)
    }
}
