//
//  RegisterFormViewController.swift
//  Appify
//
//  Created by Kapil Babani on 23/08/18.
//  Copyright © 2018 Ux_Army. All rights reserved.
//

import UIKit
import CoreData

protocol LoginInData{
    func transferUserAndPass(User:String,Pass:String)
}

class RegisterFormViewController: UIViewController, UITextFieldDelegate, UIGestureRecognizerDelegate{
    
    //MARK: - REGISTER OUTLETS
    
    @IBOutlet weak var fldname: UITextField!
    @IBOutlet weak var fldemail: UITextField!
    @IBOutlet weak var fldpassword: UITextField!
    @IBOutlet weak var fldConfirmPass: UITextField!
//    @IBOutlet weak var scrollView: UIScrollView!
    
    var delegate : LoginInData?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //keyboard show and hide notification
        
        //NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        //NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        // tap gesture recorgnizer
        
//        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.touch))
//        recognizer.numberOfTapsRequired = 1
//        recognizer.numberOfTouchesRequired = 1
//        scrollView.addGestureRecognizer(recognizer)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - SIGN IN BUTTON
    
    @IBAction func signInButtonAction(_ sender: Any) {
    let vc=self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
    self.present(vc, animated: true, completion: nil)}
    
    //MARK: - SIGNUP button
    
    @IBAction func signUpAction(_ sender: Any) {
        signUp()
    }
    
    //signUp function
    
    func signUp(){
        
        //core data intialization and entering value in entity
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let userEntity = NSEntityDescription.entity(forEntityName: "User", in: managedContext)!
        let user = NSManagedObject(entity: userEntity, insertInto: managedContext)
        
        let username = self.fldname.text
        let password = self.fldpassword.text
        let email = self.fldemail.text
        
        if (fldpassword.text!.count > 0) && (fldemail.text!.count > 0) && (fldConfirmPass.text!.count > 0) && (fldname.text!.count > 0){
            
            if(fldpassword.text == fldConfirmPass.text){
                
                // alert controller for sign up
                
                let succSignUpAlert = UIAlertController(title: fldemail.text, message: "SIGN UP successful", preferredStyle: .alert)
                let userAlreadyAlert = UIAlertController(title: fldemail.text, message: "User already exist", preferredStyle: .alert)
                
                //check if user already exist
                
                func someUserExists() -> Bool {
                    let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "User")
                    fetchRequest.predicate = NSPredicate(format: "email = %@", email!)
                    
                    var results: [NSManagedObject] = []
        
                    do {
                        results = try managedContext.fetch(fetchRequest)
                    }
                    catch {
                        print("error executing fetch request: \(error)")
                    }
                    
                    return results.count > 0
                }
                
               //checking count of current users if user alreay exist
                
                if someUserExists(){
                    let userAlreadyAction = UIAlertAction(title: "OK", style: .default)
                    userAlreadyAlert.addAction(userAlreadyAction)
                    present(userAlreadyAlert, animated: true, completion: nil)
                    print("user already exist")
                }
                else {
                    
                    //saving data to data model
                    
                    do {
                        //input data in entity on succ.
                        
                        user.setValue(self.fldname.text, forKeyPath: "name")
                        user.setValue(self.fldemail.text, forKey: "email")
                        user.setValue(self.fldpassword.text, forKey: "password")
                        try managedContext.save()
                        print("user registered")
                        
                        
                        //successful sign Up action
                        
                        let succSignUpAction = UIAlertAction(title: "OK", style: .default){(action) in
                            if (self.delegate != nil){
                                self.self.delegate?.transferUserAndPass(User: self.fldemail.text!, Pass: self.fldpassword.text!)
                            }
                            self.dismiss(animated: true, completion: nil)
                        }
                        succSignUpAlert.addAction(succSignUpAction)
                        present(succSignUpAlert, animated: true, completion: nil)
                    } catch let error as NSError {
                        print("Could not save. \(error), \(error.userInfo)")
                    }
                    
                }
            }
            else{
                print("password do not match")
                }
        }
        else{
            print("any on the field is empty")
            }
    }
    
    //MARK: - BACK
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Keyboard view control
    
    //keyboard will show
    
   // @objc   func keyboardWillShow(notification: NSNotification) {
   //     if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
   //         if self.view.frame.origin.y == 0{
   //             self.view.frame.origin.y -= keyboardSize.height - 65.0
   //         }
   //     }
   // }
    
    //keyboard will hide
    
   // @objc   func keyboardWillHide(notification: NSNotification) {
   //     if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
   //         if self.view.frame.origin.y != 0 {
   //             self.view.frame.origin.y = 0.0
   //         }
   //     }
   // }
    
    //hide keyboard on view tap
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        hideKeyboard()
    }
    
    //keyboard hide function
    
    func hideKeyboard(){
        fldemail.resignFirstResponder()
        fldpassword.resignFirstResponder()
        fldname.resignFirstResponder()
        fldConfirmPass.resignFirstResponder()
    }
    
    //on return hide keyboard
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == fldname {
            textField.resignFirstResponder()
            fldemail.becomeFirstResponder()
        } else if textField == fldemail {
            textField.resignFirstResponder()
            fldpassword.becomeFirstResponder()
        } else if textField == fldpassword {
            textField.resignFirstResponder()
            fldConfirmPass.becomeFirstResponder()
        }else if textField == fldConfirmPass{
            textField.resignFirstResponder()
            signUp()
        }
        return true
    }
    
    // Implementing the touch(), outside of viewDidLoad:
    
    @objc func touch() {
        self.view.endEditing(true)
    }
    
}







