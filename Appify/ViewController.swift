//
//  ViewController.swift
//  Appify
//
//  Created by Kapil Babani on 14/08/18.
//  Copyright © 2018 Ux_Army. All rights reserved.
//

import UIKit
import GoogleSignIn
import CoreData

class ViewController: UIViewController, GIDSignInUIDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    var activityIndicator = UIActivityIndicatorView()
    
    //Facebook name and id
    
    var FBID = ""
    var FBNAME = ""
    
    //MARK: - view Controller OUTLETS
    
    @IBOutlet weak var btnlogin: UIButton!
    @IBOutlet weak var btnregister: UIButton!
    @IBOutlet weak var btnsignIn: GIDSignInButton!
    @IBOutlet weak var btnSignOut: UIButton!
    
    //MARK: - FBbtn and functions
    
    let loginBtn: FBSDKLoginButton = {
        let button = FBSDKLoginButton()
        button.readPermissions=["public_profile", "email"]
        return button
    }()
    
    //FB login user register
    
    func fbUserRegister(){
        
        //core data intialization and entering value in entity
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let userEntity = NSEntityDescription.entity(forEntityName: "User", in: managedContext)!
        let user = NSManagedObject(entity: userEntity, insertInto: managedContext)
        
        //check if user already exist FB
        
        func someUserExists() -> Bool {
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "User")
            fetchRequest.predicate = NSPredicate(format: "fbid = %@", FBID)
            
            var results: [NSManagedObject] = []
            
            do {
                results = try managedContext.fetch(fetchRequest)
            }
            catch {
                print("error executing fetch request: \(error)")
            }
            return results.count > 0
        }
        
        // if user alreay exist FB
        
        if someUserExists(){
            print("user already exist")
            UserDefaults.standard.set(FBID, forKey: "UserID")
            print(UserDefaults.standard.string(forKey: "UserID")!)
        }
            //saving data to data model
        else {
            do {
                //input data in entity on succ.
                user.setValue(FBNAME, forKeyPath: "name")
                user.setValue(FBID, forKey: "fbid")
                try managedContext.save()
                print("user registered")
                UserDefaults.standard.set(FBID, forKey: "UserID")
                print(UserDefaults.standard.string(forKey: "UserID")!)
            }
            catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
    }
    
    // fetch FB Profile
    
    func fetchProfile() {
        FBSDKProfile.loadCurrentProfile(completion: { profile, error in
            if profile != nil {
                if (profile?.firstName) != nil {
                    //print("Hello, \(aName)!,\(String(describing: profile?.lastName)),\(String(describing: profile?.userID))")
                    self.FBID = String(describing: profile?.userID)
                    self.FBNAME = String(describing: profile?.name)
                }
            }
        })
    }
    
    //in case of parameters
    
    func parameterFetchProfile(){
        
        let parameters = ["fields": "email, first_name, last_name, picture.type(large)"]
        FBSDKGraphRequest(graphPath: "me", parameters: parameters).start(completionHandler:){ (connection, result, error)-> Void in
            if error != nil {
                print(error!)
                return
            }
            print("FB result:- \(result!) ")
        }
    }
    
    //MARK:- View states
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().uiDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if (FBSDKAccessToken.current() != nil) {
            print("FBLOGIN")
            
            fetchProfile()
            fbUserRegister()
        }
    }
    
    
    
    //MARK: - REGISTER
    
    @IBAction func registerAction(_ sender: Any) {
        let vc=self.storyboard?.instantiateViewController(withIdentifier: "register") as! RegisterFormViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK: - GOOGLE SIGN IN
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        activityIndicator.stopAnimating()
    }
    
    // Present a view that prompts the user to sign in with Google
    
    func sign(_ signIn: GIDSignIn!,user: GIDGoogleUser!,present viewController: UIViewController!) {
        print(GIDSignIn.sharedInstance().currentUser.profile.email)
        self.present(viewController, animated: true, completion: nil)
        
    }
    
    // Dismiss the "Sign in with Google" view
    
    func sign(_ signIn: GIDSignIn!,
              user: GIDGoogleUser!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - GOOGLE SIGN OUT
    
    @IBAction func didTapSignOut(_ sender: AnyObject) {
        GIDSignIn.sharedInstance().signOut()
    }
    
}


