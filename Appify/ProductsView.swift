//
//  ProductsView.swift
//  Appify
//
//  Created by Kapil Babani on 14/08/18.
//  Copyright © 2018 Ux_Army. All rights reserved.
//

import UIKit



class ProductsView: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    @IBOutlet weak var ProductTable: UITableView!
    private var data: [String] = []
    var catImages: [UIImage] = []
    let categoriesName = ["ELECTRONICS", "MOBILES", "HOME APPLIENCES", "MEMORY", "PERSONAL", "OTHERS"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //no of categories intialized
        
        for i in 0...5{
            data.append("Category \(i)")
            print(i)
            catImages.append(UIImage(named: "Cat_\(i).jpg")!)
        }
        
        // delegate dec
        
        ProductTable.dataSource = self
        ProductTable.delegate = self    
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ProductTable.dequeueReusableCell(withIdentifier: "ProductsCell")!;
        let text = categoriesName[indexPath.row]
        let catImgView = cell.viewWithTag(1) as! UIImageView
        catImgView.image = catImages[indexPath.row]
        let label = cell.viewWithTag(2) as! UILabel
        label.text = text
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc=self.storyboard?.instantiateViewController(withIdentifier: "Category") as! CategoryViewController
        if indexPath.row == 0{
            vc.start = 1
            vc.end = 5
        }
        if indexPath.row == 1{
            vc.start = 6
            vc.end = 10
        }
        if indexPath.row == 2{
            vc.start = 11
            vc.end = 15
        }
        if indexPath.row == 3{
            vc.start = 16
            vc.end = 20
        }
        if indexPath.row == 4{
            vc.start = 21
            vc.end = 25
        }
        if indexPath.row == 5{
            vc.start = 26
            vc.end = 30
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func cartButton(_ sender: Any) {
        let cartvc = self.storyboard?.instantiateViewController(withIdentifier: "cart") as! CartView
        self.present(cartvc, animated: true, completion: nil)
    }
    
    @IBAction func CatButtonAct(_ sender: UIButton) {
        let catvc = self.storyboard?.instantiateViewController(withIdentifier: "Category") as! CategoryViewController
        catvc.start = 1
        catvc.end = 30
        self.present(catvc, animated: true, completion: nil)
    }
}
