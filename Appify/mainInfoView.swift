//
//  mainInfoView.swift
//  Appify
//
//  Created by Kapil Babani on 03/09/18.
//  Copyright © 2018 Ux_Army. All rights reserved.
//

import UIKit

class mainInfoView: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK:- Skip btn tapped
    @IBAction func skipBtnTapped(_ sender: Any) {
        let productVC=self.storyboard?.instantiateViewController(withIdentifier: "Product") as! ProductsView
        self.present(productVC, animated: true, completion: nil)
    }
}
