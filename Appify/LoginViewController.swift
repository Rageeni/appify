//
//  LoginViewController.swift
//  Appify
//
//  Created by Kapil Babani on 14/08/18.
//  Copyright © 2018 Ux_Army. All rights reserved.
//

import UIKit
import CoreData


class LoginViewController: UIViewController, UITextFieldDelegate, LoginInData{
    
    //MARK: - Login OUTLETS
    @IBOutlet weak var fldpassword: UITextField!
    @IBOutlet weak var fldemail: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //keyboard show and hide notification
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Product Button
    @IBAction func productButtonAction(_ sender: Any) {
        let vc=self.storyboard?.instantiateViewController(withIdentifier: "Category") as! CategoryViewController
        vc.start = 1
        vc.end = 30
        self.present(vc, animated: true, completion: nil)
    }
    
    // MARK: - Login Button
    @IBAction func loginAction(_ sender: Any) {
        login()
    }
    
    //login function
    
    func login(){
        //core data intialization and entering value in entity
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let userEntity = NSEntityDescription.entity(forEntityName: "User", in: managedContext)!
        let user = NSManagedObject(entity: userEntity, insertInto: managedContext)
        
        //user input email and passwor
        let password = self.fldpassword.text
        let email = self.fldemail.text
        
        //check if user and password match
        func someUserExists() -> Bool {
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "User")
            fetchRequest.predicate = NSPredicate(format: "email = %@", email!)
            fetchRequest.predicate = NSPredicate(format: "password = %@", password!)
            
            var results: [NSManagedObject] = []
            
            do {
                results = try managedContext.fetch(fetchRequest)
            }
            catch {
                print("error executing fetch request: \(error)")
            }
            return results.count > 0
        }
        print(someUserExists())//checking count of current users
        
        //successful login
        if someUserExists() 
        {   UserDefaults.standard.set(email, forKey: "UserID")
            print(UserDefaults.standard.string(forKey: "UserID")!)
            let vc=self.storyboard?.instantiateViewController(withIdentifier: "mainInfo") as!
            mainInfoView
            let succLoginAlert = UIAlertController(title: email, message: "login is successful", preferredStyle: .alert)
            let loginAction = UIAlertAction(title: "ok", style: .default){(action) in
                self.present(vc, animated: true, completion: nil)
            }
            succLoginAlert.addAction(loginAction)
            present(succLoginAlert, animated: true, completion: nil)
        }
            
            //unsuccessful login
        else{
            let failLoginAlert = UIAlertController(title: fldemail.text, message: "email or password do not match", preferredStyle: .alert)
            let failLoginAction = UIAlertAction(title: "ok", style: .default)
            failLoginAlert.addAction(failLoginAction)
            present(failLoginAlert, animated: true, completion: nil)
        }
    }
    
    //MARK: - categories button
    @IBAction func categories(_ sender: Any) {
        let vc=self.storyboard?.instantiateViewController(withIdentifier: "Product") as! ProductsView
        self.present(vc, animated: true, completion: nil)
    }
    
    // MARK: - BACK button
    @IBAction func backAction(_ sender: Any) {
        showToast(Message: "back button pressed")
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func registerBtnTapped(_ sender: Any) {
        let vc=self.storyboard?.instantiateViewController(withIdentifier: "register") as! RegisterFormViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    // MARK: - Keyboard view control
    
    //keyboard will show
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height - 65.0
            }
        }
    }
    
    //keyboard will hide
    @objc func keyboardWillHide(notification: NSNotification) {
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if self.view.frame.origin.y != 0 {
                self.view.frame.origin.y = 0.0
            }
        }
    }
    
    //hide keyboard on view tap
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        hideKeyboard()
    }
    
    //keyboard hide function
    func hideKeyboard(){
        fldemail.resignFirstResponder()
        fldpassword.resignFirstResponder()
    }
    
    //on return hide keyboard
    func textFieldShouldReturn(_ scoreText: UITextField) -> Bool {
        hideKeyboard()
        return true
    }
    
    //chnaging TXT field with sign up
    func transferUserAndPass(User:String,Pass:String){
        fldemail.text = User
        fldpassword.text = Pass
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "logToRegSegue" {
            let registerVc : RegisterFormViewController = segue.destination as! RegisterFormViewController
            registerVc.delegate = self
        }
    }
    
    
    
    //MARK:- toasted IOS
    func showToast(Message:NSString){
        let label = PaddingLabel()
        label.frame = CGRect(x: 0, y: view.frame.height-100, width: view.frame.width-50, height: 50)
        label.text = Message as String
        label.font = label.font.withSize(20)
        label.textAlignment = .center
        label.backgroundColor = UIColor.white.withAlphaComponent(1.0)
        label.numberOfLines = 0
        label.alpha = 1.0
        label.layer.cornerRadius = 5
        label.clipsToBounds = true
        label.frame.origin.x = (view.frame.width/2) - (label.frame.width/2)
        self.view.addSubview(label)
        
        UIView.animate(withDuration: 5.0, delay: 0.1, options: .curveEaseOut, animations: {
            label.alpha=0.0})
        { (isCompleted) in
            print(label.text!)
            label.removeFromSuperview()
        }
    }
    
}


